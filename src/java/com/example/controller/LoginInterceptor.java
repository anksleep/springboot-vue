//package com.example.controller;
//
//
//import com.example.utils.JwtUtil;
//import com.example.utils.ThreadLocalUtil;
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.HandlerInterceptor;
//
//import java.util.Map;
///***
// *@ 登录拦截器
// * 令牌失效，则拦截
// */
//@Component
//public class LoginInterceptor implements HandlerInterceptor {
////    @Autowired
////    private StringRedisTemplate stringRedisTemplate;
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        //令牌验证
//        String token = request.getHeader("Authorization");
//        //验证token
//        try {
//            //从redis中获取相同的token
////            ValueOperations<String, String> operations = stringRedisTemplate.opsForValue();
////            String redisToken = operations.get(token);
////            if (redisToken==null){
////                //token已经失效了
////                throw new RuntimeException();
////            }
//            Map<String, Object> claims = JwtUtil.parseToken(token);
//
//            //把业务数据存储到ThreadLocal中
//            ThreadLocalUtil.set(claims);
//            //放行
//            return true;
//        } catch (Exception e) {
//            //http响应状态码为401
//            response.setStatus(401);
//            //不放行
//            return false;
//        }
//    }
//
//    @Override
//    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
//        //清空ThreadLocal中的数据
//        ThreadLocalUtil.remove();
//    }
//}
