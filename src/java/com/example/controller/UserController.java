package com.example.controller;

import com.example.entity.Result;
import com.example.entity.User;
import com.example.service.UserService;
import com.example.utils.JwtUtil;
import com.example.utils.ThreadLocalUtil;
import com.example.utils.UpLoad;
import jakarta.validation.constraints.Pattern;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/user")
@Validated
public class UserController {

    @Autowired
    private UserService userService;

    /**
     *@ 参数校验@Pattern(regexp="^ 正则表达式 $")
     *@ Validated在controller
     *@ 返回值
     *@ 异常
     */
    @PostMapping("/register")
    public Result register(@Pattern(regexp = "^\\S{5,16}$") @RequestParam("username") String userName, @Pattern(regexp = "^\\S{5,16}$") String password) {

        //查询用户
        User u = userService.findByUserName(userName);
        if (u == null) {
            //没有占用
            //注册
            User user=new User();
            user.setUserName(userName);
            user.setPassword(password);
            userService.register(user);
            return Result.success();
        } else {
            //占用
            return Result.error("用户名已被占用");
        }
    }

    @PostMapping("/login")
    public Result<String> login(@Pattern(regexp = "^\\S{5,16}$") @RequestParam("username") String userName, @Pattern(regexp = "^\\S{5,16}$") String password) {
        System.out.println(userName);
        //根据用户名查询用户
        User loginUser = userService.findByUserName(userName);
        //判断该用户是否存在
        if (loginUser == null) {

            return Result.error("用户名错误");
        }
        System.out.println("yong");
        //判断密码是否正确  loginUser对象中的password是密文
        if (password.equals(loginUser.getPassword())) {
            //登录成功
            System.out.println("yon33g");
//            Map<String, Object> claims = new HashMap<>();
//            claims.put("userId", loginUser.getUserId());
//            claims.put("userName", loginUser.getUserName());
//            String token = JwtUtil.genToken(claims);
            return Result.success("ok");
        }
        return Result.error("密码错误");
    }

    @GetMapping("/userInfo")
    public Result<User> userInfo(/*@RequestHeader(name = "Authorization") String token*/) {
        //根据用户名查询用户
       /* Map<String, Object> map = JwtUtil.parseToken(token);
        String username = (String) map.get("username");*/
        Map<String, Object> map = ThreadLocalUtil.get();
        String username = (String) map.get("username");
        User user = userService.findByUserName(username);
        return Result.success(user);
    }

    @PutMapping("/update")
    public Result update(@RequestBody @Validated User user) {
        userService.update(user);
        return Result.success();
    }
//
//    @PatchMapping("updateAvatar")
//    public Result updateImg(@RequestParam @URL String avatarUrl) {
//        userService.updateAvatar(avatarUrl);
//        return Result.success();
//    }

    @PatchMapping("/updatePwd")
    public Result updatePwd(@RequestBody Map<String, String> params,@RequestHeader("Authorization") String token) {
        //1.校验参数
        String oldPwd = params.get("old_pwd");
        String newPwd = params.get("new_pwd");
        String rePwd = params.get("re_pwd");

        if (!StringUtils.hasLength(oldPwd) || !StringUtils.hasLength(newPwd) || !StringUtils.hasLength(rePwd)) {
            return Result.error("缺少必要的参数");
        }

        //原密码是否正确
        //调用userService根据用户名拿到原密码,再和old_pwd比对
        Map<String,Object> map = ThreadLocalUtil.get();
        String username = (String) map.get("username");
        User loginUser = userService.findByUserName(username);
        if (!loginUser.getPassword().equals(oldPwd)){
            return Result.error("原密码填写不正确");
        }

        //newPwd和rePwd是否一样
        if (!rePwd.equals(newPwd)){
            return Result.error("两次填写的新密码不一样");
        }

        //2.调用service完成密码更新
        userService.updatePwd(newPwd);
        //删除redis中对应的token
        return Result.success();
    }
    @RequestMapping("/test")
    public String test()
    {
        return  "test";
    }


    public Result<String> registerinfo(String username, String password, MultipartFile file ,int userSex,String userExplain) {
        User user = userService.findByUserName(username);
        user.setUserImg(UpLoad.uploadfile(file));
return Result.success();

    }
}
