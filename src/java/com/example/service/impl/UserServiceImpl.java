package com.example.service.impl;

import com.example.entity.User;
import com.example.mapper.UserMapper;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author andy
 * @since 2023-12-15 11:12:42
 */
@Service
public class UserServiceImpl  implements UserService {
    @Autowired
    private UserMapper userMapper;


    @Override
    public User findByUserName(String userName) {
        return userMapper.getUserByName(userName);
    }

    @Override
    public void update(User user) {

    }

    public int register(User user) {
return userMapper.insert(user);
    }

    @Override
    public void updatePwd(String newPwd) {

    }


//
//    @Override
//    public void register(String username, String password) {
//        //加密
//        String md5String = Md5Util.getMD5String(password);
//        //添加
//        userMapper.add(username,md5String);
//    }
//
//    @Override
//    public void update(User user) {
//        user.setUpdateTime(LocalDateTime.now());
//        userMapper.update(user);
//    }
//
//    @Override
//    public void updateAvatar(String avatarUrl) {
//        Map<String,Object> map = ThreadLocalUtil.get();
//        Integer id = (Integer) map.get("id");
//        userMapper.updateAvatar(avatarUrl,id);
//    }
//
//    @Override
//    public void updatePwd(String newPwd) {
//        Map<String,Object> map = ThreadLocalUtil.get();
//        Integer id = (Integer) map.get("id");
//        userMapper.updatePwd(Md5Util.getMD5String(newPwd),id);
//    }
}
