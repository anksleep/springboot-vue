package com.example.service.impl;

import com.example.entity.Address;
import com.example.mapper.AddressMapper;
import com.example.service.AddressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author andy
 * @since 2023-12-15 11:12:42
 */
@Service
public class AddressServiceImpl implements AddressService {

}
