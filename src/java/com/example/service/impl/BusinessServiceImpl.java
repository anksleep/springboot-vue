package com.example.service.impl;

import com.example.entity.Business;
import com.example.mapper.BusinessMapper;
import com.example.service.BusinessService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author andy
 * @since 2023-12-15 11:12:42
 */
@Service
public class BusinessServiceImpl implements BusinessService {

}
