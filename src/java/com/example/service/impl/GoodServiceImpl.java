package com.example.service.impl;

import com.example.entity.Good;
import com.example.mapper.GoodMapper;
import com.example.service.GoodService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author andy
 * @since 2023-12-15 11:12:42
 */
@Service
public class GoodServiceImpl implements GoodService {

}
