package com.example.service;

import com.example.entity.User;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author andy
 * @since 2023-12-15 11:12:42
 */
public interface UserService  {

   User findByUserName(String userName);

    void update(User user);

    int register(User user);

    void updatePwd(String newPwd);

}
