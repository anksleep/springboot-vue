package com.example.entity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author andy
 * @since 2023-12-15 11:12:42
 */
public class Setting implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer setId;

    /**
     * 设定信息
     */
    private Integer setName;

    public Integer getSetId() {
        return setId;
    }

    public void setSetId(Integer setId) {
        this.setId = setId;
    }

    public Integer getSetName() {
        return setName;
    }

    public void setSetName(Integer setName) {
        this.setName = setName;
    }

    @Override
    public String toString() {
        return "Setting{" +
            "setId = " + setId +
            ", setName = " + setName +
        "}";
    }
}
