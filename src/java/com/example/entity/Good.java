package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author andy
 * @since 2023-12-15 11:12:42
 */
public class Good implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "goodId", type = IdType.AUTO)
    private Integer goodId;

    private String goodName;

    private String goodExplain;

    private String goodImg;

    private BigDecimal goodPrice;

    private Integer businessId;

    private String remarks;

    /**
     * 评价
     */
    private String comment;

    public Integer getGoodId() {
        return goodId;
    }

    public void setGoodId(Integer goodId) {
        this.goodId = goodId;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getGoodExplain() {
        return goodExplain;
    }

    public void setGoodExplain(String goodExplain) {
        this.goodExplain = goodExplain;
    }

    public String getGoodImg() {
        return goodImg;
    }

    public void setGoodImg(String goodImg) {
        this.goodImg = goodImg;
    }

    public BigDecimal getGoodPrice() {
        return goodPrice;
    }

    public void setGoodPrice(BigDecimal goodPrice) {
        this.goodPrice = goodPrice;
    }

    public Integer getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "Good{" +
            "goodId = " + goodId +
            ", goodName = " + goodName +
            ", goodExplain = " + goodExplain +
            ", goodImg = " + goodImg +
            ", goodPrice = " + goodPrice +
            ", businessId = " + businessId +
            ", remarks = " + remarks +
            ", comment = " + comment +
        "}";
    }
}
