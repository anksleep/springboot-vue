package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author andy
 * @since 2023-12-15 11:12:42
 */
public class Address implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "adId", type = IdType.AUTO)
    private Integer adId;

    private String contactName;

    private Integer contactSex;

    private String contactTel;

    private String address;

    private String userId;

    public Integer getAdId() {
        return adId;
    }

    public void setAdId(Integer adId) {
        this.adId = adId;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public Integer getContactSex() {
        return contactSex;
    }

    public void setContactSex(Integer contactSex) {
        this.contactSex = contactSex;
    }

    public String getContactTel() {
        return contactTel;
    }

    public void setContactTel(String contactTel) {
        this.contactTel = contactTel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Address{" +
            "adId = " + adId +
            ", contactName = " + contactName +
            ", contactSex = " + contactSex +
            ", contactTel = " + contactTel +
            ", address = " + address +
            ", userId = " + userId +
        "}";
    }
}
