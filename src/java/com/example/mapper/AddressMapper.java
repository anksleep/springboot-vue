package com.example.mapper;

import com.example.entity.Address;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author andy
 * @since 2023-12-15 11:12:42
 */
public interface AddressMapper extends BaseMapper<Address> {

}
