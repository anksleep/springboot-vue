package com.example.mapper;

import com.example.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author andy
 * @since 2023-12-15 11:12:42
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
    @Select("select * from user where userName=#{userName}")
User getUserByName(String userName);
}
