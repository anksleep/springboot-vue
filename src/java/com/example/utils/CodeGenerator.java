package com.example.utils;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import java.util.Collections;

public class CodeGenerator {

    public static void main(String[] args) {
        genecode("user", "com.example");
        genecode("business", "com.example");
        genecode("good", "com.example");
        genecode("order", "com.example");
        genecode("address", "com.example");
        genecode("setting", "com.example");
    }


    public static void genecode(String tableName, String filePath) {

        FastAutoGenerator.create("jdbc:mysql://localhost:3306/tmall", "root", "123456")
                .globalConfig(builder -> {
                    builder.author("andy") // 设置作者
//                            .enableSwagger() // 开启 swagger 模式
//                             .fileOverride() // 覆盖已生成文件 已过时到strategyConfig 中去配置
                            .dateType(DateType.ONLY_DATE)
                            .commentDate("yyyy-MM-dd HH:mm:ss")
                            .outputDir("C:\\Users\\Administrator\\IdeaProjects\\Project-Tmall\\src\\main\\java"); // 指定输出目录

                })
                .packageConfig(builder -> {
                    builder.parent(filePath) // 设置父包名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, "C:\\Users\\Administrator\\IdeaProjects\\Project-Tmall\\src\\main\\resources\\mapper")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude(tableName).entityBuilder().enableFileOverride()
                            .serviceBuilder().enableFileOverride()
                            .mapperBuilder().enableFileOverride()
                            .controllerBuilder().enableFileOverride()
                            .enableRestStyle();  // 开启生成@RestController 控制器

                    ;
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }
}